.. _docker-install:

==============================
Mailman 3 in Docker Containers
==============================

Abhilash Raj maintains container images for Mailman 3 which you can use directly
without having to go through all the steps to download dependencies and
configuring Mailman. You can find the `detailed documentation`_ on how to use
them. If you have any issues using the container images, please report them
directly on the `Github repo for docker-mailman`_.


.. _detailed documentation: https://asynchronous.in/docker-mailman/
.. _Github repo for docker-mailman: https://github.com/maxking/docker-mailman
